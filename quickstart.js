// Create New Folder
function createNewFolder(parentFolderId) {
    return new Promise(function (resolve, reject) {
        const fileMetadata = {
            'name': 'New Folder',
            'mimeType': 'application/vnd.google-apps.folder',
            'parents': [parentFolderId]
        };
        gapi.client.drive.files.create({
            resource: fileMetadata,
        }).then(function (response) {
            switch (response.status) {
                case 200:
                    const { result } = response;
                    const { id } = result;
                    const folderId = id;
                    resolve(folderId);
                    break;
                default:
                    reject('No ID was found');
                    break;
            }
        }).catch(err => reject(err));
    });
}
// Create New Doc
function createNewDoc(folderid) {
    return new Promise(function (resolve, reject) {
        var parentId = folderid;
        var fileMetadata = {
            'name': 'New document',
            'mimeType': 'application/vnd.google-apps.document',
            'parents': [parentId]
        };
        gapi.client.drive.files.create({
            resource: fileMetadata,
        }).then(function (response) {
            switch (response.status) {
                case 200:
                    const file = response.result;
                    resolve(file)
                    break;
                default:
                    reject('Error creating the document, ' + response);
                    break;
            }
        }).catch(err => reject(err));
    });
}
// Insert Content In Created Doc
function updateFileContent(fileid) {
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.responseType = 'json';
        xhr.onreadystatechange = function () {
            if (xhr.readyState != XMLHttpRequest.DONE) {
                return;
            }
        };
        xhr.open('PATCH', 'https://www.googleapis.com/upload/drive/v3/files/' + fileid + '?uploadType=media');
        xhr.setRequestHeader('Authorization', 'Bearer ' + gapi.auth.getToken().access_token);
        // Make content blob
        var firstName = document.getElementById("firstname").value;
        var lastname = document.getElementById("lastname").value;
        var content = ['firstname: ' + firstName + '\n' + 'lastname: ' + lastname];
        var contentBlob = new Blob([content], {
            'type': 'text/plain'
        });
        xhr.onload = () => {
            if (xhr.status >= 200 && xhr.status < 300) {
                resolve(content);
            } else {
                reject(xhr.statusText);
            }
        };
        xhr.onerror = () => reject(xhr.statusText);
        // send content to file
        xhr.send(contentBlob)
        
        // .then(function (response) {
        //     // check the status
        //     if (response.status == 200) {
        //         // Resolve the promise with the response
        //         const succes = response.result;
        //         resolve(succes);
        //     }
        //     else {
        //         // reject with the status text
        //         reject('error during data writing', response);
        //         //   reject(Error(xhr.statusText));
        //     }
        // }).catch(err => reject(err));


    });
}
// Do all functions in loop
function doAll() {
    createNewFolder('1TIA5AqD2JqRlVcagGVaW4GGLVK4XPe0v').then((result) => {
        console.log("new folder:", result)
        return createNewDoc(result)
    }).then((result) => {
        console.log("new file:", result)
        return updateFileContent(result.id)
    }).then((result) => {
        console.log("content", result)
    }).catch(err => console.log(err))
}